<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-factory-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientInterface;
use Stringable;

/**
 * ClientFactoryInterface interface file.
 * 
 * This interface specifies how to create new http clients.
 * 
 * @author Anastaszor
 */
interface ClientFactoryInterface extends Stringable
{
	
	/**
	 * Builds a new ClientInterface based on the given internal configuration.
	 * 
	 * @return ClientInterface
	 */
	public function createClient() : ClientInterface;
	
}
